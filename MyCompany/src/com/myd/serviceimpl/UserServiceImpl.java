package com.myd.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myd.dao.UserMapper;
import com.myd.entity.User;
import com.myd.service.UserService;
@Service("userServiceImpl")
public class UserServiceImpl implements UserService {

	@Autowired
	public UserMapper userMapper;
	
	@Override
	public List<User> getUser() {
		System.out.println("service");
		return this.userMapper.getUser();
	}

}
