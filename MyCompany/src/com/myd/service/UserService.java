package com.myd.service;

import java.util.List;

import com.myd.entity.User;

public interface UserService {
	
	/**
	 * @param id
	 * @return
	 */
	List<User> getUser();
}
