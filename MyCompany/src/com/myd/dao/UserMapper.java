package com.myd.dao;

import java.util.List;

import com.myd.entity.User;


public interface UserMapper {
	/**
	 * 根据id获取用户
	 * @param id
	 * @return User
	 */
	List<User> getUser();
}
