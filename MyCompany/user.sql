/*
Navicat MySQL Data Transfer

Source Server         : MySql
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : mybatis

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2015-09-10 11:25:09
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_pk` varchar(32) NOT NULL,
  `fk_roles_pk` varchar(255) DEFAULT NULL,
  `fk_station_pk` varchar(255) DEFAULT NULL,
  `user_birthday` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_intime` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_number` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_phone` varchar(255) DEFAULT NULL,
  `user_sex` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO user VALUES ('402880304e33e46e014e34087e440000', '5ac47af54e9502f4014e95f0406c0000', 'q', '2015-06-27T00:00:00', '123823@qq.com', '2015-06-27T00:00:00', '魏正', 'HX20105718', '11', '13381086775', '男');
INSERT INTO user VALUES ('ff8081814e62293e014e627e8c050000', '5ac47af54e9502f4014e95f0406c0000', 'r', '2015-07-06T00:00:00', 'myd521@qq.com', '2015-07-06T00:00:00', '马玉', '211', '123', '13381086775', '男');
INSERT INTO user VALUES ('ff8081814e70a670014e70a700c60000', '5ac47af54e9502f4014e95f0406c0000', '', '2015-07-09T00:00:00', 'myd521@qq.com', '2015-07-06T00:00:00', 'cs', 'cs', 'cs', '13381086775', '女');
